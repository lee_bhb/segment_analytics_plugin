import 'package:flutter/material.dart';
import 'package:segment_analytics_plugin/segment_analytics.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Segment Analytics"),
        ),
        body: const MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    const segmentWriteKey = "your segment write key";
    const commonParams = {
      "x": "1080",
      "y": 1920,
      "widget": "152g",
      "root": false,
      "price_info": {
        "price": 2998.99,
        "unit": "HK\$",
      },
    };
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          TextButton(
            onPressed: () {
              SegmentAnalyticsServices.initial(segmentWriteKey).then((value) {
                debugPrint(
                    "Analytics: Initialized successfully. instance: $value");
              });
            },
            child: const Text("Initial"),
          ),
          TextButton(
            onPressed: () {
              SegmentAnalyticsServices.screen(
                "Plugin-Test-Screen",
                category: "ABCD",
                params: commonParams,
              );
            },
            child: const Text("Test-Screen"),
          ),
          TextButton(
            onPressed: () {
              SegmentAnalyticsServices.track(
                "Plugin-Test-Track",
                params: commonParams,
              );
            },
            child: const Text("Test-Track"),
          ),
          TextButton(
            onPressed: () {
              SegmentAnalyticsServices.group(
                "Plugin-Test-Group",
                params: commonParams,
              );
            },
            child: const Text("Test-Group"),
          ),
          TextButton(
            onPressed: () {
              SegmentAnalyticsServices.identify(
                "Plugin-Test-Identify",
                params: commonParams,
              );
            },
            child: const Text("Test-Identify"),
          ),
        ],
      ),
    );
  }
}
