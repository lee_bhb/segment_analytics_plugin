package com.esdlife.segment_analytics_plugin.segment_analytics_plugin.analytics

object SegmentConstants {
    const val SEGMENT_WRITE_KEY = "segment_write_key"
    const val USER_ID = "user_id"
    const val NAME = "name"
    const val TITLE = "title"
    const val CATEGORY = "category"
    const val GROUP_ID = "group_id"
    const val PARAMS = "params"

    object Method{
        const val INITIAL = "initial"
        const val SCREEN = "screen"
        const val IDENTIFY = "identify"
        const val GROUP = "group"
        const val TRACK = "track"
    }
}