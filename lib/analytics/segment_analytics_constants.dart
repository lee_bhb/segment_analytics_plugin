class SegmentAnalyticsConstants {
  static const segmentWriteKey = "segment_write_key";
  static const userId = "user_id";
  static const name = "name";
  static const title = "title";
  static const category = "category";
  static const groupId = "group_id";
  static const params = "params";
}

enum SegmentAnalyticsMethod {
  init(methodName: "initial"),
  identify(methodName: "identify"),
  track(methodName: "track"),
  screen(methodName: "screen"),
  group(methodName: "group");

  const SegmentAnalyticsMethod({
    required this.methodName,
  });

  final String methodName;
}
