import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:segment_analytics_plugin/analytics/segment_analytics_constants.dart';

import 'segment_analytics_plugin_platform_interface.dart';

/// An implementation of [SegmentAnalyticsPluginPlatform] that uses method channels.
class MethodChannelSegmentAnalyticsPlugin
    extends SegmentAnalyticsPluginPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('segment_analytics_plugin');

  @override
  Future<dynamic> initial(String segmentWriteKey) {
    return methodChannel
        .invokeMethod<dynamic>(SegmentAnalyticsMethod.init.methodName, {
      SegmentAnalyticsConstants.segmentWriteKey: segmentWriteKey,
    });
  }

  @override
  Future<void> identify(
    String userId, {
    Map<String, dynamic>? params,
  }) {
    return methodChannel.invokeMethod(
      SegmentAnalyticsMethod.identify.methodName,
      {
        SegmentAnalyticsConstants.userId: userId,
        SegmentAnalyticsConstants.params: params,
      },
    );
  }

  @override
  Future<void> track(
    String name, {
    Map<String, dynamic>? params,
  }) {
    return methodChannel.invokeMethod(
      SegmentAnalyticsMethod.track.methodName,
      {
        SegmentAnalyticsConstants.name: name,
        SegmentAnalyticsConstants.params: params,
      },
    );
  }

  @override
  Future<void> screen(
    String title, {
    String? category,
    Map<String, dynamic>? params,
  }) {
    return methodChannel.invokeMethod(
      SegmentAnalyticsMethod.screen.methodName,
      {
        SegmentAnalyticsConstants.title: title,
        SegmentAnalyticsConstants.category: category,
        SegmentAnalyticsConstants.params: params,
      },
    );
  }

  @override
  Future<void> group(
    String groupId, {
    Map<String, dynamic>? params,
  }) {
    return methodChannel.invokeMethod(
      SegmentAnalyticsMethod.group.methodName,
      {
        SegmentAnalyticsConstants.groupId: groupId,
        SegmentAnalyticsConstants.params: params,
      },
    );
  }
}
