import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'segment_analytics_plugin_method_channel.dart';

abstract class SegmentAnalyticsPluginPlatform extends PlatformInterface {
  /// Constructs a SegmentAnalyticsPluginPlatform.
  SegmentAnalyticsPluginPlatform() : super(token: _token);

  static final Object _token = Object();

  static SegmentAnalyticsPluginPlatform _instance = MethodChannelSegmentAnalyticsPlugin();

  /// The default instance of [SegmentAnalyticsPluginPlatform] to use.
  ///
  /// Defaults to [MethodChannelSegmentAnalyticsPlugin].
  static SegmentAnalyticsPluginPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [SegmentAnalyticsPluginPlatform] when
  /// they register themselves.
  static set instance(SegmentAnalyticsPluginPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }


  Future<dynamic> initial(String segmentWriteKey) {
    throw UnimplementedError('initial() has not been implemented.');
  }

  Future<void> identify(
      String userId, {
        Map<String, dynamic>? params,
      }) {
    throw UnimplementedError('identify() has not been implemented.');
  }

  Future<void> track(
      String name, {
        Map<String, dynamic>? params,
      }) {
    throw UnimplementedError('track() has not been implemented.');
  }

  Future<void> screen(
      String title, {
        String? category,
        Map<String, dynamic>? params,
      }) {
    throw UnimplementedError('screen() has not been implemented.');
  }

  Future<void> group(
      String groupId, {
        Map<String, dynamic>? params,
      }) {
    throw UnimplementedError('group() has not been implemented.');
  }

}
