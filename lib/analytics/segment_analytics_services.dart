import 'segment_analytics_plugin_platform_interface.dart';

class SegmentAnalyticsServices {
  SegmentAnalyticsServices._();

  static Future<dynamic> initial(String segmentWriteKey) {
    return SegmentAnalyticsPluginPlatform.instance.initial(segmentWriteKey);
  }

  static Future<void> identify(
    String userId, {
    Map<String, dynamic>? params,
  }) {
    return SegmentAnalyticsPluginPlatform.instance
        .identify(userId, params: params);
  }

  static Future<void> track(
    String name, {
    Map<String, dynamic>? params,
  }) {
    return SegmentAnalyticsPluginPlatform.instance.track(
      name,
      params: params,
    );
  }

  static Future<void> screen(
    String title, {
    String? category,
    Map<String, dynamic>? params,
  }) {
    return SegmentAnalyticsPluginPlatform.instance.screen(
      title,
      category: category,
      params: params,
    );
  }

  static Future<void> group(
    String groupId, {
    Map<String, dynamic>? params,
  }) {
    return SegmentAnalyticsPluginPlatform.instance.group(
      groupId,
      params: params,
    );
  }
}
