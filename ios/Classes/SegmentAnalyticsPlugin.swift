import Flutter
import UIKit

public class SegmentAnalyticsPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "segment_analytics_plugin", binaryMessenger: registrar.messenger())
    let instance = SegmentAnalyticsPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
      guard SegmentAnalyticsService.shared.execute(methodName: call.method, arguments: call.arguments) else {
          result(FlutterMethodNotImplemented)
          return
      }
      result(true)
  }
}
